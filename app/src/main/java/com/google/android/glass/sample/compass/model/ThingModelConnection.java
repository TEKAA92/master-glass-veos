package com.google.android.glass.sample.compass.model;



import com.google.android.glass.sample.compass.util.AndroidClient;

import org.thingmodel.IWarehouseObserver;
import org.thingmodel.Thing;
import org.thingmodel.ThingType;
import org.thingmodel.Warehouse;

import java.io.Console;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import websockets.Client;

/**
 * Created by aslakw on 10.09.2014.
 */
public class ThingModelConnection {

    AndroidClient client;
    URI thingModelServerURI;
    Warehouse warehouse;
    public ArrayList<Thing> things;

    public ThingModelConnection() {
        warehouse = new Warehouse();

        things = new ArrayList<Thing>();

        client = new AndroidClient("GlassThingModelAdapter", "wss://master-bridge.eu/thingmodel/glass", warehouse);

        warehouse.RegisterObserver(new IWarehouseObserver() {
            @Override
            public void New(Thing thing, String s) {
                things.add(thing);
            }

            @Override
            public void Deleted(Thing thing, String s) {

            }

            @Override
            public void Updated(Thing thing, String s) {

            }

            @Override
            public void Define(ThingType thingType, String s) {

            }
        });



    }

}
