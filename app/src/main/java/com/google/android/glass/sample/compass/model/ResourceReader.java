/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.android.glass.sample.compass.model;
import com.google.android.glass.sample.compass.util.MathUtils;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;

import org.thingmodel.Location;
import org.thingmodel.Property;
import org.thingmodel.Thing;

import java.util.ArrayList;
import java.util.List;

/**
 * This class provides access to a list of hard-coded landmarks (located in
 * {@code res/raw/landmarks.json}) that will appear on the compass when the user is near them.
 */
public class ResourceReader {

    private static final String TAG = ResourceReader.class.getSimpleName();

    private ThingModelConnection thingModelConnection;
    private Context mContext;

    /**
     * The threshold used to display a landmark on the compass.
     */
    private static final double MAX_DISTANCE_KM = 10;
    private static final double CLUSTERING_DISTANCE_KM = 0.05;

    /**
     * The list of landmarks loaded from resources.
     */
    private final ArrayList<Resource> mResources;

    /**
     * Initializes a new {@code Landmarks} object by loading the landmarks from the resource
     * bundle.
     */
    public ResourceReader(Context context) {
        mResources = new ArrayList<Resource>();
        populateResourceList();
        mContext = context;
    }

    /**
     * Gets a list of landmarks that are within ten kilometers of the specified coordinates. This
     * function will never return null; if there are no locations within that threshold, then an
     * empty list will be returned.
     */
    public List<ResourceDrawer> getNearbyResources(double latitude, double longitude) {
        Log.e("ERROR", "test");
        ArrayList<ResourceDrawer> nearbyResources = new ArrayList<ResourceDrawer>();

        for (Resource knownResource : mResources) {

            double distanceFromUser = distanceBetween(latitude, longitude, knownResource);

            if(distanceFromUser <= MAX_DISTANCE_KM) {
                for(ResourceDrawer dr : nearbyResources){

                    double distanceFromCluster;
                    distanceFromCluster = distanceBetween(dr.getLocation(), knownResource);

                    if( distanceFromCluster <= CLUSTERING_DISTANCE_KM) {
                        dr.addResource(knownResource);
                        knownResource = null;
                        break;
                    }
                }

                if(knownResource != null) {
                    ResourceDrawer rd;
                    rd = new ResourceDrawer(mContext, knownResource, knownResource.getType());
                    rd.addResource(knownResource);
                    nearbyResources.add(rd);
                }
            }
        }
        return nearbyResources;
    }


    private double distanceBetween(double latitude, double longitude, Resource res2){
        return MathUtils.getDistance(latitude, longitude, res2.getLatitude(), res2.getLongitude());
    }

    private double distanceBetween(Resource res1, Resource res2){
        return MathUtils.getDistance(res1.getLatitude(), res1.getLongitude(), res2.getLatitude(), res2.getLongitude());
    }

    /**
     * Populates the internal places list from places found in a JSON string. This string should
     * contain a root object with a "landmarks" property that is an array of objects that represent
     * places. A place has three properties: name, latitude, and longitude.
     */
    private void populateResourceList() {
        thingModelConnection = new ThingModelConnection();


        try {
            Thread.sleep(5000);                 //1000 milliseconds is one second.
        } catch(InterruptedException ex) {
            Thread.currentThread().interrupt();
        }

        for (int i = 0; i < thingModelConnection.things.size(); i++) {
            Thing thing = thingModelConnection.things.get(i);
            String name = thing.getId();

            Property.Location.LatLng latLng = thing.getProperty("location", Property.Location.LatLng.class);
            String type = thing.getType().getName().toLowerCase();
            String prop = "";
            if(thing.getProperty("name") != null){
                prop = thing.getProperty("name").ValueToString().toLowerCase();
                Log.e("NAVN", prop);
            }

            if (latLng != null) {

                double latitude = latLng.getValue().getLatitude();
                double longitude = latLng.getValue().getLongitude();

                if (prop.contains("explosion")) {
                    Patient place = new Patient(latitude, longitude, name, "patient");
                    mResources.add(place);
                }
                if (prop.contains("police")) {
                    Unit place = new Unit(latitude, longitude, name, "unit", Color.BLUE);
                    mResources.add(place);

                }
                if (prop.contains("health")) {
                    Unit place = new Unit(latitude, longitude, name, "unit", Color.GREEN);
                    mResources.add(place);
                }
                if (prop.contains("fire")) {
                    Unit place = new Unit(latitude, longitude, name, "unit", Color.RED);
                    mResources.add(place);

                }
                //Resource place = new Resource(latitude, longitude, name, "");
                //mResources.add(place);
            }
        }
    }


}
