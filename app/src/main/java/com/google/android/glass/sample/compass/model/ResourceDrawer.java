package com.google.android.glass.sample.compass.model;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.os.Build;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.genyus.pacpie.chart.library.PieChart;
import com.genyus.pacpie.chart.library.PieDetailsItem;
import com.google.android.glass.sample.compass.R;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by hansrodtang on 05/11/14.
 */
public class ResourceDrawer {
    private List<Resource> resources = new ArrayList<Resource>();
    private ImageView view;

    private List<PieDetailsItem> piepieces = new ArrayList<PieDetailsItem>();
    private Bitmap mBackgroundImage;
    private Bitmap mOutputImage;
    private Context mContext;
    private PieChart piechart;
    private Resource mainResource;
    private String type;

    private int maxCount = 0;

    public ResourceDrawer(Context context, Resource mainResource, String type) {
        this.mContext = context;
        this.mainResource = mainResource;
        this.type = type;

        mOutputImage = Bitmap.createBitmap(150, 150, Bitmap.Config.ARGB_8888);
        mBackgroundImage = Bitmap.createBitmap(150, 150, Bitmap.Config.ARGB_8888);

        piechart = new PieChart(context);

        if (mainResource instanceof Unit) {
            piechart.setLayoutParams(new ViewGroup.LayoutParams(200, 200));
            piechart.setGeometry(200, 200, -120, -63, -100, -30, 2130837504);
        } else {
            piechart.setLayoutParams(new ViewGroup.LayoutParams(150, 150));
            piechart.setGeometry(150, 150, 0, 0, 0, 0, 2130837504);

        }

        piechart.setSkinparams(android.R.color.black);

    }

    public void addResource(Resource res) {
        resources.add(res);

        //create a slice
        PieDetailsItem item = new PieDetailsItem();
        item.count = 1;
        item.label = res.getName();
        item.color = res.getColor();
        piepieces.add(item);
        maxCount = maxCount + item.count;


        piechart.setData(piepieces, maxCount);
        piechart.invalidate();

        piechart.draw(new Canvas(mBackgroundImage));

        if (mainResource instanceof Unit) {
            mOutputImage = getMaskedBitmap(mContext.getResources(), mBackgroundImage.copy(Bitmap.Config.ARGB_8888, true), R.drawable.shield);
        } else {
            mOutputImage = mBackgroundImage;
        }
    }

    public static Bitmap getMaskedBitmap(Resources res, Bitmap source, int maskResId) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            options.inMutable = true;
        }
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        Bitmap bitmap;
        if (source.isMutable()) {
            bitmap = source;
        } else {
            bitmap = source.copy(Bitmap.Config.ARGB_8888, true);
            source.recycle();
        }
        bitmap.setHasAlpha(true);

        Canvas canvas = new Canvas(bitmap);
        Bitmap mask = BitmapFactory.decodeResource(res, maskResId);
        Paint paint = new Paint();
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
        canvas.drawBitmap(mask, 0, 0, paint);
        //mask.recycle();
        return bitmap;
    }

    public void addResources(List<Resource> res) {
        resources = res;
    }

    public Bitmap getBitmap() {
        return mOutputImage;
    }

    public String getType() {
        return this.type;
    }

    public Resource getLocation() {
        return this.mainResource;
    }
}
