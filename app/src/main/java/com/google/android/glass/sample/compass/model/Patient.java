package com.google.android.glass.sample.compass.model;

import android.content.Context;
import android.graphics.Color;

/**
 * Created by hansrodtang on 04/11/14.
 */
public class Patient extends Resource{
    public Patient(double latitude, double longitude, String name, String type) {
        super(latitude, longitude, name, type);

    }

    public int getColor() {return Color.GREEN;}

}

