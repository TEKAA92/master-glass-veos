package com.google.android.glass.sample.compass.model;

import android.content.Context;
import android.graphics.Color;

/**
 * Created by hansrodtang on 04/11/14.
 */
public class Unit extends Resource{
    private int cl;

    public Unit(double latitude, double longitude, String name, String type, int cl) {
        super(latitude, longitude, name, type);
        this.cl = cl;
    }

    public int getColor() {return cl;}
}