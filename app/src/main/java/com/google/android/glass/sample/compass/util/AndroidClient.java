package com.google.android.glass.sample.compass.util;

import com.google.protobuf.InvalidProtocolBufferException;
import com.koushikdutta.async.ByteBufferList;
import com.koushikdutta.async.DataEmitter;
import com.koushikdutta.async.callback.DataCallback;
import com.koushikdutta.async.http.AsyncHttpClient;
import com.koushikdutta.async.http.WebSocket;
import org.thingmodel.Warehouse;
import org.thingmodel.proto.FromProtobuf;
import org.thingmodel.proto.ProtoModelObserver;
import org.thingmodel.proto.ProtoTransaction;
import org.thingmodel.proto.ToProtobuf;

public class AndroidClient {

    public String senderID;
    private String _path;
    private final Warehouse _warehouse;
    private ToProtobuf _toProtobuf;
    private FromProtobuf _fromProtobuf;

    private final ProtoModelObserver _thingModelObserver;

    public AndroidClient(String senderID, String path, Warehouse warehouse) {
        this.senderID = senderID;

        _path = path;
        _warehouse = warehouse;
        _thingModelObserver = new ProtoModelObserver();
        warehouse.RegisterObserver(_thingModelObserver);

        _fromProtobuf = new FromProtobuf(warehouse);
        _toProtobuf = new ToProtobuf();

        connect();
    }

    public void connect() {
        AsyncHttpClient.getDefaultInstance().websocket(_path, null, new AsyncHttpClient.WebSocketConnectCallback() {
            @Override
            public void onCompleted(Exception e, WebSocket webSocket) {
                if (e != null) {
                    e.printStackTrace();
                    return;
                }

                webSocket.setDataCallback(new DataCallback() {
                    @Override
                    public void onDataAvailable(DataEmitter dataEmitter, ByteBufferList byteBufferList) {
                        byte[] buffer = byteBufferList.getAllByteArray();

                        try {
                            ProtoTransaction.Transaction t = ProtoTransaction.Transaction.parseFrom(buffer);
                            String senderName = _fromProtobuf.Convert(t);

                            _toProtobuf.ApplyThingsSuppressions(_thingModelObserver.Deletions
                                    .values());
                            _thingModelObserver.Reset();

                            System.out.println(senderID + " | Binary message from : "
                                    + senderName);

                        } catch (InvalidProtocolBufferException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
    }
}

